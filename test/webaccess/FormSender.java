/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webaccess;

import com.thoughtworks.selenium.DefaultSelenium;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


/**
 *
 * @author gomes
 */
public class FormSender {

    private WebDriver driver;

    @BeforeClass
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
    }

// Search using keyword through Google search
    @Test
    public void testFormSender() throws Exception {
        //Open Home Page
        driver.get("http://www.google.com");
        //Enter text in search box
        driver.findElement(By.name("q")).sendKeys("selenium");
        Thread.sleep(1000);
        //Click Search button
        driver.findElement(By.name("btnG")).click();
        Thread.sleep(10000);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
    
}
