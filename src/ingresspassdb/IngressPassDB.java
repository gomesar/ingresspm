///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package ingresspassdb;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.Scanner;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.swing.JFileChooser;
//
///**
// *
// * @author Alexandre
// */
//public class IngressPassDB {
//    private String url = "";
//    private boolean insertConfirmation = true;
//    ArrayList<Passcode> passcode_list;
//
//    public IngressPassDB() {
//        this.passcode_list = new ArrayList<>();
//    }
//
//    public IngressPassDB(String url) {
//        try {
//            this.passcode_list = loadIngressPassDB(url);
//            this.url = url;
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(IngressPassDB.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public boolean insertPasscode(String code) {
//        String codePadronized = code.toLowerCase().replaceAll("\\s+$", "");
//
//        if (!codeAlreadyRedeemed(codePadronized)) {
//            this.passcode_list.add(new Passcode(passcode_list.size(), codePadronized));
//            if (insertConfirmation) {
//                System.out.println("Passcode OK");
//            }
//            return true;
//        }
//
//        if (insertConfirmation) {
//            System.out.println("Passcode Already Redeemed");
//        }
//        return false;
//    }
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        // TODO code application logic here
//
//        IngressPassDB idb = new IngressPassDB("C:\\Users\\Alexandre\\Documents\\NetBeansProjects\\IngressPassDB\\src\\ingresspassdb\\pass.txt");
//
//        Scanner s = new Scanner(System.in);
//        int opt = -1;
//        final int SAIR_SALVANDO = 0, SAIR_SEMSALVAR = 4;
//        while (opt != 0 && opt != 4) {
//            System.out.println(String.format("%d - Salvar e sair", SAIR_SALVANDO)
//                    + "\n1 - Adicionar"
//                    + "\n2 - Testar lista"
//                    + "\n3 - DB Info"
//                    + String.format("\n%d - Sair (sem salvar)", SAIR_SEMSALVAR));
//
//            opt = s.nextInt();
//            switch (opt) {
//                case SAIR_SALVANDO: {
//                    try {
//                        idb.saveIngressPassBD();
//                    } catch (IOException ex) {
//                        Logger.getLogger(IngressPassDB.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                break;
//                case 1:
//                    System.out.println("Entre com o passcode:");
//                    String code = s.next();
//                    try {
//                        idb.insertPasscode(code);
//                    } catch (Exception e) {
//                        System.out.println(e.getMessage());
//                    }
//                    break;
//                case 2:
//                    try {
//                        idb.insertPasscodeList();
//                    } catch (Exception e) {
//                        System.out.println(e.getMessage());
//                    }
//                    break;
//                case 3:
//                    break;
//                case SAIR_SEMSALVAR:
//                    System.out.println("Saindo sem salvar alterações.");
//                    break;
//                default:
//            }
//
//        }
//
//    }
//
//    private boolean codeAlreadyRedeemed(String code) {
//        for (Passcode passcode : passcode_list) {
//            if (passcode.getCode().equals(code)) {
//                return true;
//            }
//        }
//
//        return false;
//    }
//
//    private ArrayList<Passcode> loadIngressPassDB(String url) throws FileNotFoundException {
//        Scanner s = new Scanner(new File(url));
//        ArrayList<Passcode> list = new ArrayList<>();
//        while (s.hasNextLine()) {
//            String line = s.nextLine();
////            String[] passComponents = line.split(",");
////            list.add(new Passcode(Integer.parseInt(passComponents[0]), passComponents[1]) );;
//            list.add(new Passcode(list.size(), line));
//        }
//        s.close();
//
//        return list;
//    }
//
//    private void insertPasscodeList() throws FileNotFoundException {
//        //Create a file chooser
//        JFileChooser fc = new JFileChooser();
//        fc.setCurrentDirectory(new File(System.getProperty("user.home")));
//        //In response to a button click:
//        int returnVal = fc.showOpenDialog(fc);
//        if (returnVal == JFileChooser.APPROVE_OPTION) {
//            // user selects a file
//            File selectedFile = fc.getSelectedFile();
//            System.out.println("Selected file: " + selectedFile.getAbsolutePath());
//
//            ArrayList<String> notRedeemed;
//            try (Scanner s = new Scanner(selectedFile)) {
//                notRedeemed = new ArrayList<>();
//
//                this.insertConfirmation = false;
//                while (s.hasNextLine()) {
//                    String line = s.nextLine();
//                    if (this.insertPasscode(line)) {
//                        notRedeemed.add(line.toLowerCase().replaceAll("\\s+$", ""));
//                    }
//                }
//                this.insertConfirmation = true;
//            }
//
//            System.out.println("##### ##### Passcodes inserteds:");
//            notRedeemed.stream().forEach((codeNotRedeemed) -> {
//                System.out.println(codeNotRedeemed);
//            });
//            System.out.println("##### #####");
//        }
//    }
//
//    public void saveIngressPassBD() throws IOException {
//        try (FileWriter arq = new FileWriter(this.url)) {
//            PrintWriter gravarArq = new PrintWriter(arq);
//
////        gravarArq.printf("+--Resultado--+%n");
//            for (Passcode passcode : this.passcode_list) {
//                gravarArq.printf("%s%n", passcode.getCode());
//            }
////        gravarArq.printf("+-------------+%n");
//        }
//
//    }
//}
