/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ingresspassdb;

/**
 *
 * @author Alexandre
 */
public class Passcode {
    private int id;
    private String code;

    public Passcode(int id, String code) {
        this.id = id;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }
    
}
