/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ingresspassdb;

import java.util.ArrayList;

/**
 *
 * @author Alexandre
 */
public class IngressDB {
    private ArrayList<String> passcodeList;
    private String fileFullPath;

    public IngressDB() {
        this.passcodeList = new ArrayList<>();
        this.fileFullPath = "";
    }
    
    public IngressDB(ArrayList<String> passcodeList, String fileFullPath) {
        this.passcodeList = passcodeList;
        this.fileFullPath = fileFullPath;
    }

    public ArrayList<String> getPasscodeList() {
        return passcodeList;
    }

    public void setPasscodeList(ArrayList<String> passcodeList) {
        this.passcodeList = passcodeList;
    }

    public String getFileFullPath() {
        return fileFullPath;
    }

    public void setFileFullPath(String fileFullPath) {
        this.fileFullPath = fileFullPath;
    }
    
    
    
}
