/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ingresspassdb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexandre
 */
public class IDBController {

    public final String PASSCODE_INVALID = "Invalid passcode.",
            PASSCODE_ALREADY_REDEEMED = "Passcode already redeemed.",
            PASSCODE_SUCCEFULL = "";
    public final String MSG_SUCCESSFULL = "Success.",
            MSG_FAILED = "Failed.",
            MSG_ERROR = "Error.";
    // ### 
    private IngressDB idb = null;
    private boolean insertConfirmation = true;
    private final IngressDBDao idbDao;

    public IDBController() {
        this.idbDao = new IngressDBDao();
    }

    public String newIDB(String directory) {
        try {
            if (directory.equals("")) {
                //Create a file chooser
                String fileName = JOptionPane.showInputDialog("New file name:");
                // user selects a file
                File new_file = new File(String.format("%s.txt", fileName));
                System.out.println("Created file: " + new_file.getAbsolutePath());
                directory = new_file.getAbsolutePath();
            }
            this.idb = new IngressDB();
            this.idb.setFileFullPath(directory);

            return MSG_SUCCESSFULL;
        } catch (Exception e) {
            Logger.getLogger(IDBController.class.getName()).log(Level.SEVERE, null, e);
            return MSG_ERROR;
        }
    }

    public String loadIDB(String directory) {
        try {
            if (directory.equals("")) {
                //Create a file chooser
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(new File(System.getProperty("user.home")));
                //In response to a button click:
                int returnVal = fc.showOpenDialog(fc);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    // user selects a file
                    File selectedFile = fc.getSelectedFile();
                    System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                    directory = selectedFile.getAbsolutePath();
                }
            }
            this.idb = idbDao.loadIngressDB(directory);

            return MSG_SUCCESSFULL;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IDBController.class.getName()).log(Level.SEVERE, null, ex);
            return MSG_ERROR;
        }
    }

    public String saveIDB() {
        if (idb != null) {
            try {
                idbDao.saveIngressDB(idb);
                return MSG_SUCCESSFULL;
            } catch (IOException ex) {
                Logger.getLogger(IDBController.class.getName()).log(Level.SEVERE, null, ex);
                return MSG_ERROR;
            }
        } else {
            return MSG_FAILED;
        }
    }

    public String insertPasscode(String passcode) {
        String insertPasscodeConfirmation = idbDao.insertPasscode(idb, passcode);

        if (insertPasscodeConfirmation.equals(MSG_SUCCESSFULL)) {
            if (insertConfirmation) {
                System.out.println("Passcode OK");
            }
            return MSG_SUCCESSFULL;
        } else {
            return insertPasscodeConfirmation;
        }
    }

    public String insertPasscodeList(ArrayList<String> passcodeList) {
        String resp = "%s(%d2). %s(%d2). %s(%d2).";

        // @TODO REMOVE {
        if (passcodeList == null) {

        }
        // } @TODO REMOVE

        int count_success = 0, count_fails = 0, count_erros = 0;
        for (String passcode : passcodeList) {
            String insertPasscodeConfirmation = this.insertPasscode(passcode);
            switch (insertPasscodeConfirmation) {
                case MSG_SUCCESSFULL:
                    count_success++;
                    break;
                case MSG_FAILED:
                    count_fails++;
                    break;
                case MSG_ERROR:
                    count_erros++;
                    break;
            }
        }

        return String.format(resp,
                MSG_SUCCESSFULL, count_success,
                MSG_FAILED, count_fails,
                MSG_ERROR, count_erros);
    }

    public boolean isCodeAlreadyRedeemed(String passcode) {
        return idbDao.isCodeAlreadyRedeemed(idb, passcode);
    }

    public ArrayList<Boolean> isCodeListAlreadyRedeemed(ArrayList<String> passcodeList) {
        return idbDao.isCodeListAlreadyRedeemed(idb, passcodeList);
    }

    // ##
    public ArrayList<String> getPasscodeList() {
        return this.idb.getPasscodeList();
    }

    public String getIDBFileFullPath() {
        return this.idb.getFileFullPath();
    }

    public String getPasscodeListString() {
        String resp = "";

        for (String passcode : idb.getPasscodeList()) {
            resp += String.format("%s%n", passcode);
        }

        return resp;
    }

    /* #########################################################################
     *  ###############################  DAO  ###################################
     */
    private class IngressDBDao {

        public IngressDBDao() {
        }

        public IngressDB loadIngressDB() {
            return new IngressDB();
        }

        public IngressDB loadIngressDB(String fileFullPath) throws FileNotFoundException {
            IngressDB idb = new IngressDB();
            idb.setFileFullPath(fileFullPath);
            idb.setPasscodeList(loadIngressPassDB(fileFullPath));

            return idb;
        }

        public void saveIngressDB(IngressDB idb) throws IOException {
            FileWriter arq = new FileWriter(idb.getFileFullPath());
            PrintWriter gravarArq = new PrintWriter(arq);

            for (String passcode : idb.getPasscodeList()) {
                gravarArq.printf("%s%n", passcode);
            }
            arq.close();
        }

        public boolean isCodeAlreadyRedeemed(IngressDB idb, String passcode) {
            if (idb != null) {
                System.out.println("NOT null");
                String formatedPasscode = formatPasscode(passcode);
                for (String passcodeCmp : idb.getPasscodeList()) {
//                System.out.println(String.format("(%s).equals(%s)", passcodeCmp, formatedPasscode));
                    if (passcodeCmp.equals(formatedPasscode)) {
                        return true;
                    }
                }
            }
            return false;
        }

        public ArrayList<Boolean> isCodeListAlreadyRedeemed(IngressDB idb, ArrayList<String> passcodeList) {
            ArrayList<Boolean> resp = new ArrayList<>(passcodeList.size());

            for (int i = 0; i < passcodeList.size(); i++) {
                if (isCodeAlreadyRedeemed(idb, passcodeList.get(i))) {
                    resp.add(true);
                } else {
                    resp.add(false);
                }
            }
            return resp;
        }

        private ArrayList<String> loadIngressPassDB(String url) throws FileNotFoundException {
            Scanner s = new Scanner(new File(url));
            ArrayList<String> list = new ArrayList<>();
            while (s.hasNextLine()) {
                String line = s.nextLine();
                list.add(line);
            }
            s.close();

            return list;
        }

        private String formatPasscode(String passcodeUnformated) {
            return passcodeUnformated.toLowerCase().replaceAll("\\s+$", "");
        }

        private boolean isAValidPasscode(String passcode) {
            return true;
        }

        protected String insertPasscode(IngressDB idb, String passcode) {
            String formatedPasscode = formatPasscode(passcode);
            System.out.println(String.format("Try to insert (%s)", formatedPasscode));
            if (!isCodeAlreadyRedeemed(idb, formatedPasscode)) {
                if (isAValidPasscode(passcode)) {
                    idb.getPasscodeList().add(formatedPasscode);
                    return MSG_SUCCESSFULL;
                } else {
                    return PASSCODE_INVALID;
                }
            } else {
                return PASSCODE_ALREADY_REDEEMED;
            }
        }
    }
}
