/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webaccess;

import ingresspassdb.IDBController;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.server.SeleniumServer;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 *
 * @author gomes
 */
public class FormSender {
    public final String
            MSG_INVALID = "Invalid :|",
            MSG_ALREADY_REDEEMED = "Already used :(",
            MSG_SUCCEFULL = "Success ;)";
            ;
    private final String 
            REDEEMING_PASSWORD = "Redeeming passcode...",
            PASSWORD_REDEEMED = "Passcode already redeemed.",
            INVALID_PASSCODE = "Invalid passcode.",
            SUCCESS_PASSCODE = "Passcode fully redeemed.";
    private WebDriver driver;
    private WebElement passcodeForm, passcodeSendButton;

    public FormSender() {
        try {
//            SeleniumServer serv = new SeleniumServer();
        } catch (Exception ex) {
            Logger.getLogger(FormSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
   
    public void setUp() throws Exception {
        try{
            URL url = new File("http://localhost:7055/hub").toURI().toURL();
            driver = new RemoteWebDriver(url, DesiredCapabilities.firefox() );
            System.out.println("Janela reutilizada");
        } catch(Exception e){
            driver = new FirefoxDriver();
            System.out.println("Nova janela");
        }
        driver.get("https://www.ingress.com/intel");
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("header_passcode")));
        driver.findElement(By.id("header_passcode")).click();
        passcodeForm = driver.findElement(By.id("passcode"));
        passcodeSendButton = driver.findElement(By.id("submit"));
        System.out.println("Setup Ok");
    }

// Search using keyword through Google search
    public void testFormSender() throws Exception {
        //Open Home Page
        
        //Enter text in search box
//        driver.findElement(By.name("q")).sendKeys("selenium");
        
        Thread.sleep(1000);
        //Click Search button
        driver.findElement(By.name("btnG")).click();
        Thread.sleep(10000);
    }
    
    public String submitPasscode(String passcode) throws InterruptedException{
        passcodeForm.sendKeys(passcode);
        Thread.sleep(1000);
        passcodeSendButton.click();
        
        String response = driver.findElement(By.className("rr_status_msg")).getText();
        Thread.sleep(1000);
        while(response.equals(REDEEMING_PASSWORD)){
            response = driver.findElement(By.className("rr_status_msg")).getText();
        }
        System.out.println("Resp: \"" + response + "\"");
        
        if (response.equals(INVALID_PASSCODE)){
            return MSG_INVALID;
        }
        if(response.equals(PASSWORD_REDEEMED)){
            return MSG_ALREADY_REDEEMED;
        }
        
        return MSG_SUCCEFULL;
    }

    public void tearDown() throws Exception {
        driver.quit();
    }
    
//    public static void main(String args[]){
//        FormSender fs = new FormSender();
//        try{
//            fs.setUp();
////            fs.testFormSender();
//            fs.submitPasscode("7pd3alaricw6x2y");
//            Thread.sleep(5000);
////            fs.tearDown();
//        } catch (Exception e){
//            System.out.println(e.getMessage());
//        }
//    }
    
}
